import sys
import os.path
from cx_Freeze import setup, Executable

folder = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(folder, 'src'))

from lsq.common import APP_NAME, __version__


# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    'includes': ('atexit', 'sqlalchemy', 'sqlalchemy.dialects.sqlite', \
                 'PySide.QtCore', 'PySide.QtGui', 'PySide.QtUiTools', 'PySide.QtXml'),
    'packages': [],
    'excludes': ['PySide.QtNetwork'],
    'include_files': ['data'],
    'icon': 'data/consuldoku.ico',
    'create_shared_zip': True,
    'include_msvcr': True
}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

setup(
    name = APP_NAME,
    version = __version__,
    description = 'Hồ sơ lãnh sự',
    author = 'Nguyễn Hồng Quân',
    author_email = 'ng.hong.quan@gmail.com',
    options = {
        'build_exe': build_exe_options
    },
    executables = [Executable('src/consuldoku', base=base,
                              shortcutName=APP_NAME,
                              shortcutDir='DesktopFolder'),
                   Executable('src/tools/01_createdb.py', targetName='createdb.exe')]
)
