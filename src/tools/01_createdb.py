#!/usr/bin/env python3

# This tool is to create DB file. The file is saved in data folder

import sys
import os.path
import xml.etree.ElementTree as ET

# Add parent folder to PYTHONPATH, to be able to import our package

try:
    folder = os.path.dirname(os.path.abspath(__file__))
except NameError:
    folder = os.path.dirname(sys.executable)

sys.path.append(os.path.dirname(folder))

from lsq.dbinit import engine
from lsq.models import Base
from lsq.dbinit import session
from lsq import util
from lsq.models import Country, DocType

# Make Windows command prompt print UTF-8
if sys.platform == 'win32':
    import codecs
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout.detach())

# Create tables
print('>> Drop old tables')
Base.metadata.drop_all(engine)
print('>> Create new tables')
Base.metadata.create_all(engine)


# Add countries
print('>> Add countries')
data_dir = util.get_data_dir()
isofile = os.path.join(data_dir, 'iso3166.xml')

root = ET.parse(isofile).getroot()
countries = []
top = []  # Contain Viet Nam and Malaysia
for child in root:
	orig = child.attrib
	if 'date_withdrawn' in orig:
		continue
	data = {}
	try:
		data['code'] = orig['alpha_3_code']
	except KeyError:
		data['code'] = orig.get('alpha_4_code') or orig.get('alpha_2_code')
	if not data['code']:
		continue
	data['name'] = orig['name'] if 'common_name' not in orig else orig['common_name']
	if data['name'] in ('Viet Nam', 'Malaysia'):
		top.append(data)
	else:
		countries.append(data)

top.reverse()
top[0]['name'] = 'Việt Nam'

for country in top + countries:
	print(country)
	session.add(Country(**country))


session.commit()
