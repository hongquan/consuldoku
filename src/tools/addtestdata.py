#!/usr/bin/env python3

import sys
import os.path

# Add parent folder to PYTHONPATH, to be able to import our package
folder = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(folder))

from datetime import date
from lsq.dbinit import session
from lsq import util
from lsq.models import Client

clients = (
    {
        'name': 'Nguyễn Hồng Quân',
        'gender': 'male',
        'passport': 'B221133',
        'phone': '093 9030 338',
        'nationality_id': 1,
        'birthday': date(1987, 1, 27),
        'email': 'ng.hong.quan@gmail.com',
        'address': '100, 3/2, Cần Thơ,\nViệt Nam'
    },
    {
        'name': 'Thu Tuyền',
        'passport': '9385734',
        'phone': '09123456',
        'nationality_id': 2,
    },
    {
        'name': 'Thu Hà',
        'passport': '9658930',
        'phone': '012385776'
    },
    {
        'name': 'Lệ Thủy',
        'passport': 'A974662',
        'phone': '083779500'
    }
)

[session.add(Client(**u)) for u in clients]
session.commit()
