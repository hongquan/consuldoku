import os.path
import sys

from .common import PKG_NAME, package_path, user_data_dir

def is_running_from_source():
    parent_name = os.path.basename(os.path.dirname(package_path))
    return (parent_name == 'src')

def get_data_dir():
    if is_running_from_source():
        p = os.path.join(package_path, '..', '..', 'data')
        return os.path.normpath(p)
    # Running from frozen environment (by cx_freeze)
    # Ref: http://cx_freeze.readthedocs.org/en/latest/faq.html#problems-with-running-frozen-programs
    if getattr(sys, 'frozen', False):
        folder = os.path.dirname(sys.executable)
        return os.path.join(folder, 'data')
    # Return folder in user's dir
    elif sys.platform == 'linux':
        prefix = os.path.expanduser('~/.local/share')
        return os.path.join(prefix, user_data_dir)
    elif sys.platform == 'win32':
        # TODO
        return user_data_dir
    else:
        # TODO
        return user_data_dir

def get_db_path():
    return os.path.join(get_data_dir(), PKG_NAME + '.db')

def get_ui_file(name):
    return os.path.join(get_data_dir(), name + '.ui')

def get_icon_file(name):
    for e in ('.png', '.svg'):
        path = os.path.join(get_data_dir(), name+e)
        if os.path.exists(path):
            return path
