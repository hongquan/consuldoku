# Database models

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy.types import Integer, String, Date, Boolean, Enum
from sqlalchemy.orm import relationship, backref

from .dbinit import session

Base = declarative_base()

class MixIn:
     def __int__(self):
          return self.id

     def __str__(self):
          try:
               return self.name
          except AttributeError:
               return self.__repr__()


class Client(MixIn, Base):
     __tablename__ = 'clients'
     id = Column(Integer, primary_key=True)
     name = Column(String, index=True, nullable=False)
     birthday = Column(Date)
     gender = Column(Enum('male', 'female'))
     passport = Column(String, nullable=False, unique=True)
     email = Column(String)
     phone = Column(String, nullable=False)
     address = Column(String)
     nationality_id = Column(Integer, ForeignKey('countries.id'))
     nationality = relationship('Country')

     def __str__(self):
          return '{} ({})'.format(self.name, self.id)

     def __repr__(self):
          return '<Client({} {})>'.format(self.id, self.name)


class Country(MixIn, Base):
     __tablename__ = 'countries'
     id = Column(Integer, primary_key=True)
     code = Column(String, unique=True)
     name = Column(String, unique=True)


class DocType(MixIn, Base):
     __tablename__ = "doctypes"
     id = Column(Integer, primary_key=True)
     name = Column(String)
     order_show = Column(Integer)

     def __init__(self, name, order=None):
          self.name = name
          if type(order) is int:
               self.order_show = order

     def __repr__(self):
          return '<DocType({})>'.format(self.name)


class Document(Base):
     __tablename__ = "documents"
     id = Column(Integer, primary_key=True)
     name = Column(String, index=True)
     issuedate = Column(Date)
     client_id = Column(Integer, ForeignKey('clients.id'))
     client = relationship('Client',
                           backref=backref('documents', order_by=id,
                                           cascade='all, delete, delete-orphan'))
     doctype_id = Column(Integer, ForeignKey('doctypes.id', ondelete='CASCADE'))
     doctype = relationship(DocType,
                            backref=backref('documents', order_by=id,
                                           cascade='all, delete, delete-orphan'))
     docno = Column(String, index=True, unique=True)

     def __repr__(self):
          return '<Document({})>'.format(self.name or self.docno)



def get_all_doctypes():
     return ((row.id, row.name)
             for row in session.query(DocType).order_by('order_show'))


def update_all_doctypes(dtlist):
     updated = []
     for id, name, order in dtlist:
          dt = session.query(DocType).get(id)
          if not dt: # New
               dt = DocType(name, order)
               session.add(dt)
               session.commit()
               updated.append(dt.id)
               continue
          if dt.name != name:
               dt.name = name
          if dt.order_show != order:
               dt.order_show = order
          updated.append(id)
     # Now check which ID is absent in dtlist, remove from DB
     updated = frozenset(updated)
     indb = frozenset(i for (i,) in session.query(DocType.id))
     to_remove = indb - updated
     if len(to_remove):
          session.query(DocType).filter(DocType.id.in_(to_remove)).delete(False)
          session.flush()
     session.commit()


def get_all_countries():
     for row in session.query(Country):
          yield row.id, row.code, row.name


def find_clients_like(text=None):
     if not text:
          return tuple(session.query(Client))
     return tuple(session.query(Client).filter(Client.name.like('%{}%'.format(text))))


def get_client(uid):
     return session.query(Client).get(uid)


def get_client_docs(client):
     return session.query(Document).filter_by(client=client)


def get_doc(did):
     return session.query(Document).get(did)


def add_client(data):
     try:
          nat_id = data.pop('nationality')
          data['nationality_id'] = nat_id
     except KeyError:
          pass
     u = Client(**data)
     session.add(u)
     session.commit()
     return u


def update_client(client, data):
     try:
          nat_id = data.pop('nationality')
          data['nationality_id'] = nat_id
     except KeyError:
          pass
     session.query(Client).filter_by(id=client.id).update(data)
     session.commit()
     return client


def delete_client(client):
     session.delete(client)
     session.flush()
     session.commit()


def passport_exist(passport):
     return bool(session.query(Client).filter_by(passport=passport).count())


def passport_dup(client, passport):
     others = session.query(Client).filter(Client.id != client.id)
     return bool(others.filter_by(passport=passport).count())


def add_doc(data, client):
     try:
          type_id = data.pop('doctype')
          data['doctype_id'] = type_id
     except KeyError:
          pass
     d = Document(**data)
     d.client = client
     session.add(d)
     session.commit()
     return d


def update_doc(doc, data):
     try:
          type_id = data.pop('doctype')
          data['doctype_id'] = type_id
     except KeyError:
          pass
     r = session.query(Document).filter_by(id=doc.id).update(data)
     session.commit()
     return doc


def delete_doc(doc):
     session.delete(doc)
     session.flush()
     session.commit()


def doc_exist(docno):
     return bool(session.query(Document).filter_by(docno=docno).count())


def doc_dup(doc, docno):
     others = session.query(Document).filter(Document.id != doc.id)
     return bool(others.filter_by(docno=docno).count())
