# This file is to create 1 database session,
# to user in other places of our app

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from . import util

engine = create_engine('sqlite:///' + util.get_db_path())
Session = sessionmaker(bind=engine)

session = Session()
