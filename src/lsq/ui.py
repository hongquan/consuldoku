import platform
import datetime

from PySide.QtCore import QCoreApplication, Slot, Qt, QModelIndex, QTimer
from PySide.QtGui import QLayout, QWidget, QMainWindow, QDialog
from PySide.QtGui import QLineEdit, QPlainTextEdit, QDateTimeEdit, QToolButton,\
                         QIcon, QStyle, QCompleter
from PySide.QtGui import QStandardItem, QStandardItemModel

from .pyside_dynamic import loadUi
from .util import get_ui_file, get_icon_file

# We use a technique from http://goo.gl/EnLrH
# to create a subclass of QMainWindow with widgets from
# Qt Designer file.

class ConsulDokuWindow(QMainWindow):
    clientinfo_names = ('name', 'gender', 'birthday', 'passport', 'email',
                      'nationality', 'address', 'phone')
    docinfo_names = ('doctype', 'docno', 'issuedate')
    style_error = 'color: red'
    style_sucess = 'color: green'

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        loadUi(get_ui_file('main'), self)
        self.statusbar.hide()
        self.setWindowIcon(QIcon(get_icon_file('consuldoku')))
        self.add_textedit_widgets()
        css = 'font-size: 16px; height: 22px'
        self.input_name.setStyleSheet(css)
        self.input_docno.setStyleSheet(css)
        self.set_tab_orders()
        self.set_completers()
        # An object as utility to get/set value from "edit" widgets easier
        self.input = Input(self)
        self.input.issuedate = datetime.date.today()


    def add_textedit_widgets(self):
        to_add = ('name', 'passport', 'phone')
        for k in to_add:
            self.create_insert_to_grid(k, self.gridlayout)
        self.create_insert_to_grid('docno', self.hoso)


    def create_insert_to_grid(self, name, grid):
        label = getattr(self, 'label_'+name)
        idx = grid.indexOf(label)
        if (idx == -1):
            return
        pos = grid.getItemPosition(idx)
        inp = LineEditEx()
        grid.addWidget(inp, pos[0], pos[1]+1)
        setattr(self, 'input_'+name, inp)


    def set_tab_orders(self):
        prev = self.input_name
        client_widget_names = self.clientinfo_names[0:1] + ('female', 'male') \
                            + self.clientinfo_names[2:]
        doc_widgets_names = self.docinfo_names + ('add_doc', 'edit_doc', 'remove_doc', 'exit')
        for k in client_widget_names + doc_widgets_names:
            try:
                current = getattr(self, 'input_'+k)
            except AttributeError:
                current = getattr(self, 'btn_'+k)
            self.setTabOrder(prev, current)
            prev = current


    def set_completers(self):
        self.set_name_completer()
        self.set_docno_completer()


    def set_name_completer(self):
        model = QStandardItemModel(0, 2, self)
        completer = QCompleter(self)
        completer.setModel(model)
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        # By default, QCompleter filter the suggestion by prefix (common
        # beginning characters). We don't want that. We want match at
        # any position (beginning or middle). So we disable the built-in search
        # and provide the suggestion list by our own.
        completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
        self.input_name.setCompleter(completer)
        # Create completion list each time user types
        self.input_name.textEdited.connect(self.get_name_suggestions)
        completer.activated.connect(self.load_client)


    def set_docno_completer(self):
        model = QStandardItemModel(0, 2, self)
        completer = QCompleter(self)
        completer.setModel(model)
        self.input_docno.setCompleter(completer)
        completer.activated.connect(self.load_doc)


    @property
    def app(self):
        ''' Refer to the app running this window'''
        return QCoreApplication.instance()


    # Make use of auto-connecting signal & slot of Qt Designer
    # Read more: http://goo.gl/yvVxZ
    @Slot(bool)
    def on_btn_exit_clicked(self, checked):
        # For why the "checked" parameter, see: http://ow.ly/mv0lZ
        self.app.exit()


    @Slot(bool)
    def on_btn_save_client_clicked(self, checked):
        data = {k: getattr(self.input, k) for k in self.clientinfo_names}
        r, err = self.app.update_client(data)
        if err == 'NO_CLIENT':
            self.set_client_error('Chưa chọn khách hàng')
        elif err == 'NOT_VALID':
            self.set_client_error('Thông tin thiếu hoặc không phù hợp')
        elif err == 'PASSPORT_EXIST':
            self.set_client_error('Passport/CMND trùng với khách hàng khác')
        elif r:
            self.set_client_error('Đã lưu', False)
        else:
            self.set_client_error('Lỗi {}'.format(err))


    @Slot(bool)
    def on_btn_new_client_clicked(self, checked):
        data = {k: getattr(self.input, k) for k in self.clientinfo_names}
        r, err = self.app.add_client(data)
        if err == 'NOT_VALID':
            self.set_client_error('Thông tin thiếu hoặc không phù hợp')
        elif err == 'PASSPORT_EXIST':
            self.set_client_error('Passport/CMND trùng với khách hàng cũ')


    @Slot(bool)
    def on_btn_delete_client_clicked(self, checked):
        if self.app.delete_client():
            self.set_client_error('Đã xóa', False)
            self.clear_all_edit(self.centralWidget())
            # Update suggestions
            self.set_doc_suggestions()


    @Slot(bool)
    def on_btn_add_doc_clicked(self, checked):
        data = {k: getattr(self.input, k) for k in self.docinfo_names}
        r, err = self.app.add_doc(data)
        if err == 'NOT_VALID':
            self.set_state_error('docno')
            self.set_doc_error('Thông tin thiếu hoặc không phù hợp')
        elif err == 'NO_CLIENT':
            self.set_client_error('Chưa chọn khách hàng')
        elif err == 'DOCNO_EXIST':
            self.set_doc_error('Số hồ sơ bị trùng')
        elif r:
            self.set_doc_error('Đã lưu', False)
            # Update suggestions
            self.set_doc_suggestions()
        else:
            self.set_doc_error('Lỗi {}'.format(err))


    @Slot(bool)
    def on_btn_edit_doc_clicked(self, checked):
        data = {k: getattr(self.input, k) for k in self.docinfo_names}
        r, err = self.app.update_doc(data)
        if err == 'NOT_VALID':
            self.set_state_error('docno')
            self.set_doc_error('Thông tin thiếu hoặc không phù hợp')
        elif err == 'NO_CLIENT':
            self.set_client_error('Chưa chọn khách hàng')
        elif err == 'NO_DOC':
            self.set_doc_error('Chưa chọn hồ sơ')
        elif err == 'DOCNO_EXIST':
            self.set_doc_error('Số hồ sơ bị trùng')
        elif r:
            self.set_doc_error('Đã lưu', False)
            # Update suggestions
            self.set_doc_suggestions()
        else:
            self.set_doc_error('Lỗi {}'.format(err))


    @Slot(bool)
    def on_btn_remove_doc_clicked(self, checked):
        if self.app.delete_doc():
            self.set_doc_error('Đã xóa', False)
            self.input.docno = None
            # Update suggestions
            self.set_doc_suggestions()


    @Slot(bool)
    def on_btn_today_clicked(self, checked):
        self.input.issuedate = datetime.date.today()


    @Slot(bool)
    def on_btn_clear_clicked(self, checked):
        for k in self.clientinfo_names + self.docinfo_names:
            setattr(self.input, k, None)


    @Slot(bool)
    def on_btn_man_doctype_clicked(self, checked):
        diag = DocTypeDialog(self)
        self.center_dialog(diag)
        model = self.input_doctype.model()
        self.input_doctype.setEnabled(False)
        diag.list_doctype.setModel(model)
        diag.list_doctype.setModelColumn(1)
        resp = diag.exec_()
        diag.destroy()
        if resp:
            self.update_doctypes()
        else:
            self.app.populate_doctypes()
        self.input_doctype.setEnabled(True)


    @Slot()
    def get_name_suggestions(self, text):
        if not text:
            return
        completer = self.input_name.completer()
        model = completer.model()
        model.clear()
        for name, i in self.app.get_name_suggestions(text):
            model.appendRow((QStandardItem(name), QStandardItem(str(i))))
        completer.complete()


    @Slot()
    def load_client(self, text):
        completer = self.input_name.completer()
        # This case, we use unfilter mode, so the
        # current index must be get from popup.
        index = completer.popup().currentIndex()
        model = completer.model()
        item = model.item(index.row(), 1) # The id is at column 1
        uid = int(item.text())
        client = self.app.load_client(uid)

        self.input.gender = client.gender
        self.input.passport = client.passport
        self.input.birthday = client.birthday
        self.input.email = client.email
        self.input.nationality = client.nationality
        self.input.address = client.address
        self.input.phone = client.phone
        self.set_doc_suggestions(client)


    @Slot()
    def load_doc(self, text):
        completer = self.input_docno.completer()
        # completer.currentIndex() or completer.currentRow()
        # is not accurate
        index = completer.popup().currentIndex()
        model = completer.model()
        item = model.item(index.row(), 1) # The id is at column 1
        did = int(item.text())
        doc = self.app.load_doc(did)

        self.input.doctype = doc.doctype
        self.input.docno = doc.docno
        self.input.issuedate = doc.issuedate
        if not doc.doctype:
            self.set_doc_error('Hồ sơ này chưa phân loại')


    def get_toolbutton(self, widget):
        for b in widget.children():
            if isinstance(b, QToolButton):
                return b


    def set_doc_suggestions(self, client=None):
        completer = self.input_docno.completer()
        model = completer.model()
        model.clear()
        if not client:
            client = self.app.current_client
        for i, name in self.app.get_client_docs(client):
            model.appendRow((QStandardItem(name), QStandardItem(str(i))))


    def clear_all_edit(self, widget):
        for child in widget.children():
            if isinstance(child, (QLineEdit, QPlainTextEdit)):
                child.clear()
            elif isinstance(child, QLayout):
                self.clear_all_edit(child)


    def populate_doctypes(self, dtlist):
        model = self.input_doctype.model()
        model.clear()
        # Make the model of ComboBox contain 2 column:
        # id & name of doctype.
        model.setColumnCount(2)
        # The ComboBox shows 'name' column.
        self.input_doctype.setModelColumn(1)
        for i, name in dtlist:
            model.appendRow((QStandardItem(str(i)), QStandardItem(name)))


    def populate_nationalities(self, countries):
        model = self.input_nationality.model()
        model.setColumnCount(3)
        self.input_nationality.setModelColumn(2)
        for i, code, name in countries:
            model.appendRow((QStandardItem(str(i)), QStandardItem(code),
                             QStandardItem(name)))


    def set_state_error(self, name):
        getattr(self, 'input_'+name).set_state_error()


    def apply_error_style(self, widget, iserror=True):
        old_style = widget.styleSheet()
        if iserror and old_style != self.style_error:
            widget.setStyleSheet(self.style_error)
        elif not iserror and old_style != self.style_sucess:
            widget.setStyleSheet(self.style_sucess)



    def set_client_error(self, message, iserror=True):
        self.apply_error_style(self.label_client_error, iserror)
        self.label_client_error.setText(message)
        QTimer.singleShot(3000, self.label_client_error.clear)


    def set_doc_error(self, message, iserror=True):
        self.apply_error_style(self.label_doc_error, iserror)
        self.label_doc_error.setText(message)
        QTimer.singleShot(3000, self.label_doc_error.clear)


    def doctypes_from_listview(self):
        model = self.input_doctype.model()
        for row in range(model.rowCount()):
            tid = int(model.item(row, 0).text())
            name = model.item(row, 1).text()
            yield tid, name, row


    def update_doctypes(self):
        dtlist = self.doctypes_from_listview()
        self.app.update_doctypes(dtlist)


    def center_dialog(self, dialog):
        x, y = self.pos().toTuple()
        w, h = self.size().toTuple()
        dw, dh = dialog.size().toTuple()
        rx = (w - dw)/2   # Desired relative offset
        ry = (h - dh)/2
        dialog.move(x+rx, y+ry)



class LineEditEx(QLineEdit):
    ''' QLineEdit with button '''
    def __init__(self, *args, **kwargs):
        super(LineEditEx, self).__init__(*args, **kwargs)
        self.add_button()
        self._held_signals = {}


    def add_button(self):
        self.button = btn = QToolButton(self)
        btn.setIcon(QIcon(get_icon_file('clear')))
        css = 'QToolButton {{border: none; padding:0}}'.format(self.height())
        btn.setStyleSheet(css)
        btn.setCursor(Qt.PointingHandCursor)
        framewidth = self.style().pixelMetric(QStyle.PM_DefaultFrameWidth)
        css = '{} {{padding-right: {}px}}'.format(self.__class__.__name__,
                                                  btn.sizeHint().width() + framewidth + 1)
        self.setStyleSheet(css)
        self.move_button()
        btn.clicked.connect(self.clear)

    def move_button(self):
        btn = self.button
        ww, wh = self.width(), self.height()
        bw, bh = btn.sizeHint().width(), btn.sizeHint().height()
        framewidth = self.style().pixelMetric(QStyle.PM_DefaultFrameWidth)
        btn.move(ww-bw-framewidth, (wh-bh)/2)

    # Overrided methods
    def resizeEvent(self, event):
        super(LineEditEx, self).resizeEvent(event)
        self.move_button()


    def set_state_error(self):
        self.button.setIcon(QIcon(get_icon_file('warning')))
        self.textChanged.connect(self.check_content)

    def set_state_normal(self):
        self.button.setIcon(QIcon(get_icon_file('clear')))
        self.textChanged.disconnect(self.check_content)


    @Slot()
    def check_content(self, text):
        if text:
            self.set_state_normal()



class Input:
    ''' A class as utility to get/set value from "edit" widgets easier '''
    def __init__(self, window):
        self.wd = window

    @property
    def name(self):
        return self.wd.input_name.text()

    @name.setter
    def name(self, value):
        self.wd.input_name.setText(value)


    @property
    def gender(self):
        if self.wd.btn_female.isChecked():
            return 'female'
        if self.wd.btn_male.isChecked():
            return 'male'
        return None

    @gender.setter
    def gender(self, value):
        if value is None:
            self.wd.btn_male.setChecked(False)
            self.wd.btn_female.setChecked(False)
            return
        if value not in ('female', 'male'):
            return
        getattr(self.wd, 'btn_'+value).setChecked(True)


    @property
    def birthday(self):
        return self.wd.input_birthday.date().toPython()

    @birthday.setter
    def birthday(self, date):
        if not date:
            date = datetime.date(1980, 1, 1)
        self.wd.input_birthday.setDate(date)


    @property
    def passport(self):
        return self.wd.input_passport.text()

    @passport.setter
    def passport(self, value):
        self.wd.input_passport.setText(value)


    @property
    def email(self):
        return self.wd.input_email.text()

    @email.setter
    def email(self, text):
        self.wd.input_email.setText(text)


    @property
    def nationality(self):
        row = self.wd.input_nationality.currentIndex()
        model = self.wd.input_nationality.model()
        try:
            return int(model.item(row, 0).text())
        except AttributeError:
            return None

    @nationality.setter
    def nationality(self, country):
        model = self.wd.input_nationality.model()
        try:
            item = model.findItems(str(int(country)), column=0)[0]
        except (IndexError, TypeError):
            return
        self.wd.input_nationality.setCurrentIndex(item.row())


    @property
    def address(self):
        return self.wd.input_address.toPlainText()

    @address.setter
    def address(self, value):
        self.wd.input_address.clear()
        self.wd.input_address.insertPlainText(value)


    @property
    def phone(self):
        return self.wd.input_phone.text()

    @phone.setter
    def phone(self, value):
        self.wd.input_phone.setText(value)


    @property
    def doctype(self):
        row = self.wd.input_doctype.currentIndex()
        model = self.wd.input_doctype.model()
        try:
            return int(model.item(row, 0).text())
        except AttributeError:
            return None

    @doctype.setter
    def doctype(self, dtype):
        model = self.wd.input_doctype.model()
        try:
            item = model.findItems(str(int(dtype)), column=0)[0]
        except (IndexError, TypeError):
            return
        self.wd.input_doctype.setCurrentIndex(item.row())


    @property
    def docno(self):
        return self.wd.input_docno.text()

    @docno.setter
    def docno(self, value):
        self.wd.input_docno.setText(value)


    @property
    def issuedate(self):
        return self.wd.input_issuedate.date().toPython()

    @issuedate.setter
    def issuedate(self, date):
        if not date:
            date = datetime.date.today()
        self.wd.input_issuedate.setDate(date)



class DocTypeDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        loadUi(get_ui_file('doctype_dialog'), self)
        if platform.system() != 'Linux':
            self.btn_add.setIcon(QIcon(get_icon_file('add')))
            self.btn_del.setIcon(QIcon(get_icon_file('delete')))
            self.btn_up.setIcon(QIcon(get_icon_file('up')))
            self.btn_down.setIcon(QIcon(get_icon_file('down')))


    @Slot(bool)
    def on_btn_add_clicked(self, checked):
        model = self.list_doctype.model()
        item = QStandardItem('Mới...')
        model.appendRow((QStandardItem('0'), item))
        idx = model.indexFromItem(item)
        self.list_doctype.setCurrentIndex(idx)
        self.list_doctype.edit(idx)


    @Slot(bool)
    def on_btn_del_clicked(self, checked):
        model = self.list_doctype.model()
        idx = self.list_doctype.currentIndex()
        model.removeRow(idx.row())


    @Slot(bool)
    def on_btn_up_clicked(self, checked):
        listview = self.list_doctype
        row = listview.currentIndex().row()
        if row == 0:
            # On top, cannot go up more
            return
        model = listview.model()
        items = model.takeRow(row)
        model.insertRow(row-1, items)
        listview.setCurrentIndex(items[listview.modelColumn()].index())


    @Slot(bool)
    def on_btn_down_clicked(self, checked):
        listview = self.list_doctype
        row = listview.currentIndex().row()
        dest = row + 1
        model = listview.model()
        if not model.hasIndex(dest, 0):
            # At bottom, cannot go down more
            return
        items = model.takeRow(row)
        model.insertRow(dest, items)
        listview.setCurrentIndex(items[listview.modelColumn()].index())
