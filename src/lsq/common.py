# Some common variables

import os.path

APP_NAME = 'ConsulDoku'
PKG_NAME = 'consuldoku'
__version_info__ = (1, 1)
__version__ = '.'.join(str(i) for i in __version_info__)

package_path = os.path.dirname(os.path.abspath(__file__))

user_data_dir = os.path.join(APP_NAME, 'data')
