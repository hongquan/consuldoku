import sys
import os.path
from time import gmtime, strftime
import platform
import ctypes

from PySide.QtCore import QFile
from PySide.QtGui import QMessageBox
from PySide.QtUiTools import QUiLoader
from PySide.QtGui import QApplication

from sqlalchemy.exc import IntegrityError

from .common import __version__, APP_NAME
from .util import get_ui_file
from . import models
from .models import get_all_doctypes, get_all_countries, update_all_doctypes
from .dbinit import session
from .ui import ConsulDokuWindow


class ConsulDokuApp(QApplication):
    def __init__(self, argv):
        super(ConsulDokuApp, self).__init__(argv)
        self.current_client = None
        self.current_doc = None
        self.register_windows_app()
        self.window = ConsulDokuWindow()
        self.window.setWindowTitle(self.window.windowTitle().format(__version__))
        self.populate_doctypes()
        self.populate_nationalities()
        self.window.input_name.setFocus()


    def register_windows_app(self):
        if platform.platform().startswith('Windows'):
            myappid = 'hongquan.' + APP_NAME
            try:
                ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
            except AttributeError:
                # Windows XP and older
                pass


    def set_text_today(self, text):
        self.window.label_today.setText(text)


    def populate_doctypes(self):
        dtlist = get_all_doctypes()
        self.window.populate_doctypes(dtlist)


    def populate_nationalities(self):
        countries = get_all_countries()
        self.window.populate_nationalities(countries)


    def update_doctypes(self, dtlist):
        update_all_doctypes(dtlist)


    def get_name_suggestions(self, text=None):
        clients = models.find_clients_like(text)
        return ((u.name, u.id) for u in clients)


    def get_client_docs(self, client):
        if not client:
            return ()
        docs = models.get_client_docs(client)
        return ((d.id, d.docno) for d in docs)


    def load_client(self, uid):
        client = models.get_client(uid)
        self.current_client = client
        return client


    def load_doc(self, did):
        doc = models.get_doc(did)
        self.current_doc = doc
        return doc


    def validate_client_data(self, data):
        ok = True
        for k in ('name', 'passport', 'phone'):
            if not data[k]:
                self.window.set_state_error(k)
                ok = False
        return ok


    def add_client(self, data):
        if not self.validate_client_data(data):
            return False, 'NOT_VALID'
        if models.passport_exist(data['passport']):
            return False, 'PASSPORT_EXIST'
        try:
            client = models.add_client(data)
            self.current_client = client
            return client, None
        except IntegrityError as e:
            return False, 'INTEGRITY_ERROR'


    def update_client(self, data):
        client = self.current_client
        if not client:
            return False, 'NO_CLIENT'
        if not self.validate_client_data(data):
            return False, 'NOT_VALID'
        if models.passport_dup(client, data['passport']):
            return False, 'PASSPORT_EXIST'
        try:
            client = models.update_client(client, data)
            return client, None
        except IntegrityError as e:
            print(e)
            return False, 'INTEGRITY_ERROR'


    def delete_client(self):
        client = self.current_client
        models.delete_client(client)
        self.current_client = None
        return True


    def add_doc(self, data):
        if not self.current_client:
            return False, 'NO_CLIENT'
        if not data['docno']:
            return False, 'NOT_VALID'
        if models.doc_exist(data['docno']):
            return False, 'DOCNO_EXIST'
        try:
            doc = models.add_doc(data, self.current_client)
            self.current_doc = doc
            return doc, None
        except IntegrityError:
            return False, 'INTEGRITY_ERROR'


    def update_doc(self, data):
        if not self.current_client:
            return False, 'NO_CLIENT'
        if not self.current_doc:
            return False, 'NO_DOC'
        if not data['docno']:
            return False, 'NOT_VALID'
        if models.doc_dup(self.current_doc, data['docno']):
            return False, 'DOCNO_EXIST'
        try:
            doc = models.update_doc(self.current_doc, data)
            self.current_doc = doc
            return doc, None
        except IntegrityError:
            return False, 'INTEGRITY_ERROR'


    def delete_doc(self):
        doc = self.current_doc
        models.delete_doc(doc)
        self.current_doc = None
        return True


    def run(self):
        self.window.show()
        self.window.resize(640, 480)
        return self.exec_()



