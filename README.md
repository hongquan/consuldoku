ConsulDoku
==========

Quản lý đơn từ lãnh sự quán

## Thư viện yêu cầu

1. Python3
1. python3-pyside
1. python3-sqlalchemy

## Công cụ

Thư mục _src/tools_ chứa các script hỗ trợ:

- `01_createdb.py`: Để sinh ra file database. File sinh ra sẽ nằm trong thư mục data.
Nếu build cho Windows, file sinh ra sẽ là `createdb.exe` nằm cùng thư mục với file chạy chính (không nằm trong `tools`).

- `addtestdata.py`: Thêm dữ liệu phục vụ cho test.

## Đóng gói cho Windows

- Để đóng gói, cần cài `cx_freeze` trên Windows và chạy lệnh sau từ thư mục của dự án

```
C:\Python33\python.exe setupwin.py build
```
Bản build nằm trong thư mục _build_.

- Build file cài đặt (*.msi):

```
C:\Python33\python.exe setupwin.py bdist_msi
```
File tạo thành nằm trong thư mục _dist_

- Chú ý

http://stackoverflow.com/questions/17509088/system-path-error-with-pyqt-and-py2exe

- File chạy sau khi build

    - `consuldoku.exe`

    - `createdb.exe`
